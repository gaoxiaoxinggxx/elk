﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace Zhaoxi.ELK
{
   public class TestData
    {
        

        public void IndexMay()
        {
           //创建链接管道
           var settings =  new ConnectionSettings(new Uri(Url.url)).DefaultIndex("people");
            var client = new ElasticClient(settings);
            List<Person> peoples = new List<Person>();

            for (int i = 0; i < 10; i++)
            {
                Person personinfo = new Person()
                {
                    Id = i,
                    FirstName = $"星哥{i}号",
                    LastName = $"高小星{i}号"
                };
                peoples.Add(personinfo);
            }
            client.IndexMany<Person>(peoples);
          
        }
    }

    public class Url
    {
        public static string url = "http://localhost:9200/";
    }

}
