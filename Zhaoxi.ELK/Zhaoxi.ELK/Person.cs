﻿namespace Zhaoxi.ELK
{
    public class Person
    {
        /// <summary>
        /// id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 第一个名称
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// 第二个名称
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// 添加家庭地址
        /// </summary>
        public string Address { get; set; }
    }
}